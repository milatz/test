<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'birthday' => date('+1 month'),
        'street' => date('+1 month'),
        'number' => '06-12345678',
        'zipcode' => '0000 BA',
        'city' => str_random(8),
        'note' => str_random(80),
        'remember_token' => str_random(10),
        'created_at' => NULL,
        'updated_at' => NULL,
    ];
});
