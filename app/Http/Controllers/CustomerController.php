<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role == 'kantoor'){
            return view('kantoor.customers.create');
        } else {
            return redirect('/');
        }
    }


    public function store(CustomerRequest $request)
    {
        $customer = new Customer(array(
            'name' => $request->get('name'),
            'birthday' => $request->get('birthday'),
            'street' => $request->get('street'),
            'number' => $request->get('number'),
            'zipcode' => $request->get('zipcode'),
            'city' => $request->get('city'),
            'note' => $request->get('note'),

        ));

        $customer->save();

        return redirect('customers/create')->with('status', 'Klant is aangemaakt!');
    }

    public function index()
    {
        $customer_request = Customer::all();

        if(Auth::user()->role == 'kantoor'){
            return view('kantoor.customers.index', compact('customer_request'));
        } else {
            return redirect('/');
        }
    }

    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);

        $customer->delete();

        return redirect('customers/index')->with('status', 'Klant is verwijderd!');
    }

    public function edit($id)
    {
        $customerId = Customer::find($id);

        if (Auth::user()->role == 'kantoor') {
            return view('kantoor.customers.edit', compact('customerId'));
        } else {
            return redirect('/');
        }
    }

    public function update($id, CustomerRequest $request)
    {
        $customers = Customer::find($id);

        $customers->name = $request->get('name');
        $customers->birthday = $request->get('birthday');
        $customers->street = $request->get('street');
        $customers->number = $request->get('number');
        $customers->zipcode = $request->get('zipcode');
        $customers->city = $request->get('city');
        $customers->note = $request->get('note');

        $customers->save();

        return redirect('customers/edit/'.$id)->with('status', 'Klant is gewijzigd!');
    }

    public function overview()
    {
        if(Auth::user()->role == 'kantoor'){
            return view('kantoor.customers.overview');
        } else {
            return redirect('/');
        }
    }
}
