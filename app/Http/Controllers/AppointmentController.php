<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Appointment;
use App\Customer;
use App\User;
use App\Http\Requests\AppointmentRequest;
use Illuminate\Routing\Controller;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $appointment_request = Appointment::all();

        if(Auth::user()->role == 'verloskundigen'){
            return view('verloskundigen.home', compact('appointment_request'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer_request = Customer::all();
        $verloskundigen_request = User::all();

        if(Auth::user()->role == 'kantoor' || Auth::user()->role == 'verloskundigen'){
            return view('verloskundigen.afspraken.create', compact('customer_request', 'verloskundigen_request'));
        } else {
            return redirect('/');
        }
    }

    public function store(AppointmentRequest $request)
    {
        $appointment = new Appointment(array(
            'name' => $request->get('name'),
            'midwife' => $request->get('midwife'),
            'timefrom' => $request->get('timefrom'),
            'timeto' => $request->get('timeto'),
            'note' => $request->get('note'),

        ));

        $appointment->save();

        return redirect('appointment/create')->with('status', 'Afspraak is aangemaakt!');
    }

    public function edit($id)
    {
        $appointmentId = Appointment::find($id);
        $verloskundigen = User::all();
        $customer = Customer::all();

        if(Auth::user()->role == 'kantoor' || Auth::user()->role == 'verloskundigen'){
            return view('verloskundigen.afspraken.edit', compact('appointmentId', 'verloskundigen', 'customer'));
        } else {
            return redirect('/');
        }
    }

    public function update($id, AppointmentRequest $request)
    {
        $appointments = Appointment::find($id);

        $appointments->name = $request->get('name');
        $appointments->verloskundigen = $request->get('midwife');
        $appointments->timefrom = $request->get('timefrom');
        $appointments->timeto = $request->get('timeto');
        $appointments->note = $request->get('note');

        $appointments->save();

        return redirect('appointment/edit/'.$id)->with('status', 'Afspraak is gewijzigd!');
    }
}
