@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Klant overzicht</div>

                    <div class="panel-body" style="text-align: center">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            <a href="{{ URL::route('customercreate') }}"><input type="button" class="btn btn-default btn-customer-create" value="Klant aanmaken"></a>

                            <form class="form-horizontal" action="/customers/index" method="POST" role="search">
                                {{ csrf_field() }}
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control" name="q" placeholder="Zoek klanten">
                                        <span class="customer-search">
                                            <button type="submit" class="btn btn-default">Zoeken</button>
                                        </span>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
