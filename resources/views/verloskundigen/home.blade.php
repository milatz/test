@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Agenda van verloskundigen {{ Auth::user()->name }}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 25%">Tijd</th>
                                <th>Afspraak</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appointment_request as $row)
                            <tr onClick="document.location.href='/';">
                                <td>{{ $row->timefrom }} - {{ $row->timeto }}</td>
                                <td>
                                    <strong>Naam:</strong> {{ $row->name }}
                                    <br/>
                                    <strong>Notitie:</strong> {{ $row->note }}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
