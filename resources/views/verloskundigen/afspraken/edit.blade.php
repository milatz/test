@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Afspraak wijzigen</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Naam klant</label>

                                <div class="col-md-6">
                                    <select id="name" name="name" class="form-control">
                                        <option value="{{ $appointmentId->name }}">{{ $appointmentId->name }}</option>

                                        @foreach($customer as $row)
                                            @if($row->name == $appointmentId->name)
                                            @else
                                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('verloskundigen') ? ' has-error' : '' }}">
                                <label for="verloskundigen" class="col-md-4 control-label">Verloskundigen</label>

                                <div class="col-md-6">
                                    <select id="verloskundigen" name="verloskundigen" class="form-control">
                                        <option value="{{ $appointmentId->verloskundigen }}">{{ $appointmentId->verloskundigen }}</option>
                                        @foreach($verloskundigen as $row)
                                            @if($row->role == 'verloskundigen')
                                                @if($row->name == $appointmentId->verloskundigen)
                                                    @else
                                                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                                <label for="time" class="col-md-4 control-label">Tijd van - tot</label>
                                 <?php $arr = array( "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"); ?>

                                <div class="col-md-3">
                                    <select id="time" name="timefrom" class="form-control">
                                        <option value="{{ $appointmentId->timefrom }}">{{ $appointmentId->timefrom }}</option>
                                        @foreach($arr as $row)
                                            <option value="{{ $row }}:00">{{ $row }}:00</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select id="time" name="timeto" class="form-control">
                                        <option value="{{ $appointmentId->timeto }}">{{ $appointmentId->timeto }}</option>
                                        @foreach($arr as $row)
                                            <option value="{{ $row }}:00">{{ $row }}:00</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                <label for="note" class="col-md-4 control-label">Notitie</label>

                                <div class="col-md-6">
                                    <textarea id="note" name="note" class="form-control">{{ $appointmentId->note }}</textarea>

                                    @if ($errors->has('note'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Afspraak Wijzigen
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
